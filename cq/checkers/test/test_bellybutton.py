# pylint: disable=implicit-str-concat-in-sequence
from typing import Any
import os
import pathlib

import bellybutton.cli
import bellybutton.parsing
import pytest


@pytest.fixture
def rules() -> Any:
	bellybutton_conf = os.path.join(os.path.abspath(os.path.dirname(__file__)), '../.bellybutton.yml')
	with open(bellybutton_conf, 'r', encoding = 'utf-8') as f:
		rules = bellybutton.parsing.load_config(f)
	return rules


@pytest.fixture
def tmp_file(tmp_path: Any) -> Any:
	tmp_directory = tmp_path / 'bellybutton_tmp'
	tmp_directory.mkdir()
	tmp_file = tmp_directory / 'bellybutton_tmp.txt'
	return tmp_file


@pytest.mark.parametrize(
	'line',
	[
		"logger.error(f'ahojoojo')",
		"logger.error(f'ahojoojo', whatever_function())",
		"logger.error(f'ahojoojo', whatever_variable)",
		"logger.error('ahojoojo'.format())",
		'logger.error(STRING_VARIABLE.format())',
		'logger.error(string_variable.format())',
		"logger.error('ahojoojo'.format(), whatever_variable)",
		'self._logger.error(string_variable.format())',
		'self.logger.error(string_variable.format())',
		'log.info(string_variable.format())',
		"logger.info('{}, {}'.format(1, 2))",
		'logger.error(string_variable.format(function(), variable))',
		'logger.info("{}".format(1))',
		'logger.info(' "  ''.format()" ')',
		'logger.info(' '  "".format()' ')',
		'logger.info("{}".format(1))',
		'logger.info("\'{}\'".format(1))',
		'logger.info(f\'"{a}"\')',
		'logger.info(f"\'{a}\'")',
	],
)
def test_true_positive(line: str, rules: Any, tmp_file: pathlib.Path) -> None:
	tmp_file.write_text(line)
	results = bellybutton.cli.linting_failures([tmp_file], rules)
	results_count = len(list(results))
	assert results_count == 1
