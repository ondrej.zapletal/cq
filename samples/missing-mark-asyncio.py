# type: ignore
import asyncio

import pytest


# this should be marked with @pytest.mark.asyncio, otherwise it won't run
async def test_some_testing():
	await asyncio.sleep(0)


@pytest.mark.asyncio
async def tets_x():
	await asyncio.sleep(0)


# this is an example of a function, which is asynchronous, but isn't required to be
# marked with @pytest.mark.asyncio
async def helper():
	await asyncio.sleep(0)
